#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 27 17:09:32 2023

@author: Lea Musiolek
"""
import numpy as np
import pandas as pd
import glob
import sys
import os
import matplotlib.pyplot as plt


fish_movements_path = "../Fish_movements"

rawfilepath = '/home/spathiphyllum/Desktop/Fish_datasets/Data_Jolles_et_al_2020/Data_Experiment_1/Experiment_1_raw_tracks/Live Pair/'
#prepfilepath = '../Neuroevolution_by_file/'
#outpath = './Output_Fish_simulation/'
#outpath = "../Neuroevolution/"

sys.path.append(fish_movements_path)
import fish_movements as fm
import geometry_tools as geom

#%% Global parameters

field_of_view = 3 #np.pi # one-sided absolute viewing angle of the fish in radians
fish_range_1d = 4.0 # maximum swimming distance per frame in cm in one dimension
focalfish = 0
partnerfish=1
#number_of_rays = 10 # used in raycasting
#number_of_modules = 2 # used in raycasting

#%% Neuroevolution configurations

generations = 40
selection = 10
repro_multiplier = 3
mutation_prob = 0.3
stopping_patience = 3
label_config = "FollowTrack"
coordinate_system = "polar"
testing_mode = "simulation" # "prelabeled" 
discrim_path = "../Neuroevolution/final_discriminator_trained"

