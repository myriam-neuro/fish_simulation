# Fish_simulation

Input configuration:
AllVisiWalls - Visible closest points on walls
WallsAndPartner - Visible closest points on walls, as well as partners
Partner - Partner position from agent's subjective point of view
PartnerNext - Next partner position

Loss configuration:
FollowTrack - Loss: deviation of predicted move from next move in ground truth data
ToWall - Loss: deviation of predicted move from move towards the wall
FollowPartner - Loss: deviation of predicted move from move towards current position of partner
ParallelToWall - Loss: deviation of predicted move's angle from nearest wall angle and magnitude 
    from distance to nearest wall (only works in polar encoding)
    
Output encoding:
Polar - 
Cartesian - 