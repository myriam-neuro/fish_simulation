#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 31 10:08:45 2023

@author: spathiphyllum
"""

import pickle 
import numpy as np
import glob
import math
import random
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import sys
import os
from datetime import date
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from tqdm import tqdm

import setup
sys.path.append(setup.fish_movements_path)
import fish_movements as fm
import geometry_tools as geom
import simulator as sim

#%% Add new genome
def new_genome(genomes:dict, partner_timesteps:list, wall_vision:bool, input_noise:float, hidden_layers:list, random_seed:int, 
               mutate:bool, mutation_prob:float):
    '''
    Function for creating and appending a new ANN genome to an existing dictionary of genomes.

    Parameters
    ----------
    genomes : dict
        Dictionary of pre-existing genomes
    partner_timesteps : list
        List of time steps (neg = in the past, pos=in the future) from which the ANN gets location input for a social partner
    wall_vision : bool
        Whether or not agent defined by this genome gets wall input
    input_noise : float
        Factor with which the standard deviation of training inputs is multiplied for adding Gaussian noise in training
    hidden_layers : list
        List containing the number of nodes for each hidden MLP layer. If empty, there are no hidden layers
    random_seed : int
        Random seed for MLP initialization
    mutate : bool
        Whether or not the genes given as arguments are stochastically altered in creating the new genome

    Returns
    -------
    genomes : TYPE
        Returns the dictionary of existing genomes with the new genome at the end

    '''
    newkey = len(genomes)
    if mutate:
        # Add or remove wall vision
        if np.random.binomial(1, mutation_prob) == 1:
            wall_vision = not wall_vision
            
        # Shift a partner_timestep
        if np.random.binomial(1, mutation_prob) == 1:
            partner_timesteps[random.choice([c for c in range(0, len(partner_timesteps))])] += random.choice([-1,1])
        # Add a partner_timestep
        if np.random.binomial(1, mutation_prob) == 1:
            new_timestep = random.choice([c for c in range(-150,151)])
            if new_timestep not in partner_timesteps:
                partner_timesteps.append(new_timestep)
        # Remove a partner_timestep
        if len(partner_timesteps) > 1 and np.random.binomial(1, mutation_prob) == 1:
            partner_timesteps.pop(random.randint(0, len(partner_timesteps)-1))
        
        # Decrease or increase input noise
        if np.random.binomial(1, mutation_prob) == 1:
            input_noise += random.choice([-0.2,0.2])
            if input_noise < 0:
                input_noise = 0

        # Add a hidden layer with 2 nodes
        if np.random.binomial(1, mutation_prob/3) == 1:
            hidden_layers.append(2)
            
        # Remove a hidden layer
        if len(hidden_layers) > 0 and np.random.binomial(1, mutation_prob/3) == 1:
            hidden_layers.pop(random.randint(0,len(hidden_layers)-1))
                
        # Add a node to two hidden layers
        if len(hidden_layers) > 0 and np.random.binomial(1, mutation_prob) == 1:
            hidden_layers[random.choice([c for c in range(0, len(hidden_layers))])] += 1
            hidden_layers[random.choice([c for c in range(0, len(hidden_layers))])] += 1
        

        
    genomes[newkey] = {
        "random_seed" : random_seed,
        "wall_vision" : wall_vision,
        "partner_timesteps" : partner_timesteps,
        "input_noise" : input_noise,
        "hidden_layers" : hidden_layers,
        "testing_loss" : np.nan,
        "n_parameters" : np.nan
        }
    return genomes


#%% Neuroevolution process
def fish_ann_neuroevolution(generations:int, selection:int, repro_multiplier:int, mutation_prob:float, stopping_patience:int, 
                            label_config:str, coordinate_system:str, testing_mode:str, focalfish:int, partnerfish:int,
                            field_of_view:float, fish_range_1d:float,
                            discrim_path:str, outpath:str, prepfilepath:str):
    population_size = selection*repro_multiplier
    run = str(date.today())+"_"+label_config+"_"+coordinate_system
    
    if os.path.exists(outpath+"genomes_"+run+".pkl"):
        with open(outpath+"genomes_"+run+".pkl", 'rb') as g:
            genomes = pickle.load(g)
        dead_genomes = len(genomes) - population_size
    else:
        genomes = {}
        for n in range(0, selection):
            genomes = new_genome(genomes,[0],False,0.0,[0],0,False,mutation_prob)
            for mutant in range(1, repro_multiplier):
                genomes = new_genome(genomes,[0],False,0.0,[0],mutant,True,mutation_prob)
    
        dead_genomes = 0
        
    lastgen_testloss = np.inf
    stop_counter = 0
    objectives_longterm = np.empty((0, 8),float)
    
    for generation in tqdm(range(0, generations)):
        print("\nNow starting generation "+str(generation))
        alive = {k: genomes[k] for k in range(dead_genomes,len(genomes))}
        objectives = np.empty((0, 8),float)
        
        # Model training and testing
        for key in alive:
            genome = alive[key]
            model, n_parameters = sim.model_train(genome, key, run, prepfilepath, outpath, 
                                                  focalfish, partnerfish, label_config, coordinate_system, 
                                                  field_of_view, fish_range_1d, 0.2, False)
            if testing_mode == "prelabeled":
                test_loss = sim.model_test_prelabeled(model, outpath)
            elif testing_mode == "simulation":
                test_loss = sim.model_test_sim_discrim(model, genome, key, run, prepfilepath, outpath, discrim_path, focalfish, partnerfish, coordinate_system, "with", field_of_view, "onesided")
            #del(model)
            genomes[key]["testing_loss"] = test_loss
            genomes[key]["n_parameters"] = n_parameters
            objectives = np.append(objectives, np.array([[n_parameters, test_loss, 1, np.nan, np.nan, np.nan, key, generation]]), axis=0)
            
        # Selection
        ## Find and mark Pareto optima with priority on test loss 
        objectives = objectives[objectives[:, 1].argsort()]    
        min2 = objectives[0,0]
        objectives[0,2] = 0
        for point in range(1,len(objectives)):
            if objectives[point,0] <= min2:
                min2 = objectives[point,0]
                objectives[point,2] = 0
        
        ## Rescale objectives and compute combined objective
        objectives[:,4] = geom.min_max_scaling(objectives[:,0])
        objectives[:,5] = geom.min_max_scaling(objectives[:,1])
        objectives[:,3] = objectives[:,5] + objectives[:,4]
        
        ## Sort by Pareto front membership and combined objective, choose best
        ind = np.lexsort((objectives[:,3], objectives[:,2]), axis=0)
        objectives = objectives[ind]
        
        best_genomes = objectives[0:selection,:]
        best_genome_keys = best_genomes[:,6]
        objectives_longterm = np.append(objectives_longterm, best_genomes, axis=0)
        dead_genomes += population_size
        with open(outpath+"genomes_"+run+".pkl", 'wb') as g:
            pickle.dump(genomes, g)
            
        np.savetxt(outpath+"Objectives_longterm_run_"+run+".csv", objectives_longterm, delimiter=";")
        
        # Check stopping criterion
        generational_testloss = np.mean(best_genomes[:,1])
        if generational_testloss >= lastgen_testloss:
            stop_counter += 1
            
        if stop_counter >= stopping_patience:
            print("Stopping criterion reached, ending evolution early after "+str(generation)+" generations.")
            break
            
        # Reproduction
        for key in best_genome_keys:
            genomes = new_genome(genomes, genomes[key]["partner_timesteps"], genomes[key]["wall_vision"], genomes[key]["input_noise"], 
                                 genomes[key]["hidden_layers"],0, False, mutation_prob)
            for mutant in range(1, repro_multiplier):
                genomes = new_genome(genomes, genomes[key]["partner_timesteps"], genomes[key]["wall_vision"], genomes[key]["input_noise"], 
                                     genomes[key]["hidden_layers"], mutant, True, mutation_prob)
    
    print("Reached final generation, stopping criterion not reached.")    
    
    
    #%% EVALUATING EVOLUTION RESULTS
    
    if os.path.exists(outpath+"genomes_"+run+".pkl"):
        with open(outpath+"genomes_"+run+".pkl", 'rb') as g:
            genomes = pickle.load(g)
        
    objectives_longterm = np.loadtxt(outpath+"Objectives_longterm_run_"+run+".csv", delimiter=";")
    
    # Visualize complexity and testing loss across generations
    plt.close('all')
    fig, ax1 = plt.subplots()
    plt.title("Loss and complexity across generations")
    color = 'tab:red'
    ax1.set_xlabel('Generations')
    ax1.set_ylabel('Number of parameters', color=color)
    ax1.plot(objectives_longterm[:,-1], objectives_longterm[:,0], color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax2 = ax1.twinx()  # instantiate a second axis that shares the same x-axis
    color = 'tab:blue'
    ax2.set_ylabel('Testing loss', color=color)  # we already handled the x-label with ax1
    ax2.plot(objectives_longterm[:,-1], objectives_longterm[:,1], color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(outpath+"Loss_complexity_generations_"+run+".png")
    #plt.show()
    
    plt.close('all')
    fig, ax3 = plt.subplots()
    plt.title("Testing loss as a function of complexity")
    ax3.set_xlabel('Number of parameters')
    ax3.set_ylabel('Testing loss')
    ax3.scatter(objectives_longterm[:,0], objectives_longterm[:,1], c="k", s=0.2)
    plt.savefig(outpath+"Loss_vs_complexity_"+run+".png")
    #plt.show()