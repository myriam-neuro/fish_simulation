#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 12:44:31 2023

@author: spathiphyllum
"""
import pickle
import architecture_evolution as ae
from setup import *

datfiles = [x[0]+"/" for x in os.walk('../Neuroevolution_by_file/')]
for prepfilepath in datfiles[1:]:
    outpath = prepfilepath
    ae.fish_ann_neuroevolution(generations, selection, repro_multiplier, mutation_prob, stopping_patience, 
                               label_config, coordinate_system, testing_mode, focalfish, partnerfish, field_of_view, fish_range_1d, 
                               discrim_path, outpath, prepfilepath)
    
    
with open('/home/spathiphyllum/Desktop/N11P4_complete/genomes_2023-06-26_FollowTrack_polar.pkl', 'rb') as g:
    genomes = pickle.load(g)