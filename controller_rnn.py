
'''
Myriam Hamon
April 2023
'''
import numpy as np
import math
import random
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers



def data_rnn (input, timestep, labels = False):

    '''

    :param input:
    :param labels:
    :param timestep:
    :return:input fitted for rnn

    no stride !! so no overlapping sequences so far
    '''

    print(input.shape)

    label_indices = np.arange(start=timestep, stop=len(labels), step=timestep)
    rnn_labels = labels[label_indices]

    input_rows = len(rnn_labels)  # make sure as many rows for input as for labels
    rnn_input = input[range(timestep * input_rows)]
    rnn_input = np.reshape(rnn_input, (input_rows, timestep, input.shape[1]))
    print(rnn_input.shape)



    return rnn_input, rnn_labels

def rnn_controller (timestep, random_seed, input_nodes, model_layers, nodes_per_layer, coordinate_system, fish_range_1d):


    inputs = keras.Input(shape=(timestep,input_nodes)) #changed
    hiddenlayer = inputs
    for d in range(0, model_layers):
        if model_layers>1 and d ==0:
            hiddenlayer = layers.SimpleRNN(nodes_per_layer, activation="tanh", return_sequences = True)(hiddenlayer)  # changed
        else :
            hiddenlayer = layers.SimpleRNN(nodes_per_layer, activation="tanh")(hiddenlayer)  # changed

    if coordinate_system == "cartesian":
        def relu_clipped(x):
            return keras.backend.relu(x + fish_range_1d, alpha=0, threshold=0.0,
                                      max_value=fish_range_1d * 2) - fish_range_1d

        out_x = layers.Dense(1, activation=relu_clipped)(hiddenlayer)
        out_y = layers.Dense(1, activation=relu_clipped)(hiddenlayer)

    if coordinate_system == "polar":
        def relu_clipped(x):
            return keras.backend.relu(x, alpha=0, max_value=math.sqrt(2 * (fish_range_1d ** 2)), threshold=0.0)
        out_x = layers.Dense(1, activation=relu_clipped)(hiddenlayer)
        
        def tanh_to_atan2(x):
            return keras.backend.tanh(x) * np.pi
        out_y = layers.Dense(1, activation=tanh_to_atan2)(hiddenlayer)

    outputs = layers.Concatenate(axis=1)([out_x, out_y])

    model = keras.Model(inputs=inputs, outputs=outputs, name="fish_model")
    model.summary()
    # keras.utils.plot_model(model, outpath+"model_"+entry+".png", show_shapes=True)

    opt = keras.optimizers.Adam(clipnorm=2.0)
    model.compile(loss="mean_squared_error", optimizer=opt)



    return model
