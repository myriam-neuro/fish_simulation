#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 26 11:07:42 2023

@author: spathiphyllum
"""

model_architectures = ["MLP"]
#model_architectures.extend(["RNN_"+str(timewindow) for timewindow in [5, 20, 40]])

input_configurations = ["Partner","WallsAndPartner"]  #"Raycasts", "Partner", "AllVisiWalls"
#input_configurations.extend(["PartnerNext_"+str(anticipation_timestep) for anticipation_timestep in [3,10]])
#input_configurations.extend(["WallsAndPartnerNext_"+str(anticipation_timestep) for anticipation_timestep in [3,10]])

model_configs = {}

for ma in model_architectures:
    for cs in ["polar", "cartesian"]: #
        for ic in input_configurations: #
            for lc in ["FollowTrack", "FollowPartner", "FollowTrack"]: # 
                for ml in [0,1]:
                    for npl in [2,6]:
                        for tino in [0,0.25,1]: # this parameter is only for training!
                            for rs in [1,2]:
                                model_configs[ma+"_"+ic+"_"+lc+str(ml)+str(npl)+str(tino)+cs+str(int(rs))] = {
                                    "model_arch" : ma,
                                    "input_config" : ic,
                                    "label_config" : lc,
                                    "model_layers" : ml,
                                    "nodes_per_layer" : npl,
                                    "training_input_noise" : tino,
                                    "coordinate_system" : cs,
                                    "random_seed" : rs
                                    }
