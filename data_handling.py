#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 26 11:44:41 2023

@author: spathiphyllum
"""

import glob
import sys
import os
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import setup
sys.path.append(setup.fish_movements_path)
import fish_movements as fm
import geometry_tools as geom

#%% 
def data_extract(rawfilepath:str, prepfilepath:str):
    '''Pair fish data extraction from Ethovision xlsx files. Saves to csv file.

    Parameters
    ----------
    rawfilepath : str
        DESCRIPTION.
    prepfilepath : str
        DESCRIPTION.

    Returns
    -------
    None.

    '''
    datfiles = glob.glob(rawfilepath+"*.xlsx")
    for datfile in datfiles:
        filename = datfile.split("/")[-1].split(".xlsx")[0]
        if os.path.exists(prepfilepath+filename+"_data.csv"):
            continue
        
        rawdata = pd.read_excel(io=datfile, header=None, sheet_name=0, na_values="-")
        data = rawdata[[5,6,11,12]]
        np.savetxt(prepfilepath+filename+"_data.csv", data.values, delimiter=";")


#%%
def partnerinput_prep(prepfilepath:str, focalfish:int, partnerfish:int, partner_timestep:int,
              field_of_view:float, coordinate_system:str):
    '''
    Data preprocessing for different input and label configurations. Saves to csv files.

    Parameters
    ----------
    prepfilepath : str
        DESCRIPTION.
    focalfish : int
        DESCRIPTION.
    partnerfish : int
        DESCRIPTION.
    partner_timestep : int
        DESCRIPTION.
    field_of_view : float
    label_config : str
        DESCRIPTION.
    coordinate_system : str

    Returns
    -------
    None.

    '''
    datfiles = glob.glob(prepfilepath+"*_data.csv")
    for datfile in datfiles:
        filename = datfile.split("/")[-1].split("_data.csv")[0]
        outfilepath = prepfilepath+filename+"_partner_ts"+str(partner_timestep) \
        +"_focalpos"+str(focalfish)+"_fov"+str(field_of_view)+"_"+coordinate_system+"_partnerinput.csv"
        if os.path.exists(outfilepath):
            continue

        data = np.loadtxt(datfile, delimiter=";")
        focaltruth = data[:,focalfish*2:(focalfish*2)+2]
        partnercoords = data[:,partnerfish*2:(partnerfish*2)+2]
                
        focaldispx, focaldispy, focalmagnits, focalangles = fm.framewise_velocity(focaltruth[:,0], focaltruth[:,1])
        partnerdispx, partnerdispy, partnermagnits, partnerangles = fm.framewise_velocity(partnercoords[:,0], partnercoords[:,1])

        # INPUT   
        training_input = np.empty((len(focaltruth), 2), float)
        training_input[:] = np.nan
        
        first_frame = 0
        final_frame = len(focaltruth)-1
        
        if partner_timestep < 0:
            first_frame -= partner_timestep
        elif partner_timestep > 0:
            final_frame -= partner_timestep
        
        for i in range(first_frame, final_frame):                
            current_input = fm.get_partner_input(focaltruth[i,0], focaltruth[i,1], focalangles[i], field_of_view, 
                                      coordinate_system, partnercoords[i+partner_timestep, 0], 
                                      partnercoords[i+partner_timestep, 1], partnerangles[i+partner_timestep])
                    
            training_input[i,:] = current_input

        np.savetxt(
            outfilepath, 
            training_input, delimiter=";") 
        

#%%        
def wallinput_prep(prepfilepath:str, focalfish:int, partnerfish:int, field_of_view:float, coordinate_system:str, plot_data:bool):
    datfiles = glob.glob(prepfilepath+"*_data.csv")
    for datfile in datfiles:
        filename = datfile.split("/")[-1].split("_data.csv")[0]
        outfilepath = prepfilepath+filename+"_walls_fov"+str(field_of_view)+"_"+coordinate_system+"_wallinput.csv"
        if os.path.exists(outfilepath):
            continue

        data = np.loadtxt(datfile, delimiter=";")
        focaltruth = data[:,focalfish*2:(focalfish*2)+2]
        partnercoords = data[:,partnerfish*2:(partnerfish*2)+2]
        
        corners, walls, tank = fm.tank_corners_walls(focaltruth, partnercoords)
        np.savetxt(prepfilepath+filename+"_walls_general.csv", np.array(walls), delimiter=";")
        
        if plot_data:
            cornersplot = np.append(corners, [corners[0,:]], axis=0)
            plt.close('all')
            fig=plt.figure(figsize=(5, 5), dpi= 100, facecolor="w", edgecolor="k")
            plt.box(on=False)
            plt.title("Original data")
            plt.plot(focaltruth[:,0], focaltruth[:,1], c="g", lw=0.5)
            plt.plot(partnercoords[:,0], partnercoords[:,1], c="b", lw=0.5)
            plt.plot(cornersplot[:,0], cornersplot[:,1], c="k", lw=0.5)            
            g_patch = mpatches.Patch(color="g", label="Focal fish")
            b_patch = mpatches.Patch(color="b", label="Partner fish")
            plt.legend(handles=[g_patch, b_patch], loc="center", bbox_to_anchor=(0.1, 0))
            plt.savefig(prepfilepath+filename+"_plot.png", dpi=500)
                
        focaldispx, focaldispy, focalmagnits, focalangles = fm.framewise_velocity(focaltruth[:,0], focaltruth[:,1])

        # INPUT   
        training_input = np.empty((len(focaltruth), 8), float)
        training_input[:] = np.nan
        
        first_frame = 0
        final_frame = len(focaltruth)-1
                
        for i in range(first_frame, final_frame):                
            current_input = fm.get_wall_input(focaltruth[i,0], focaltruth[i,1], focalangles[i], field_of_view, coordinate_system, walls)
                    
            training_input[i,:] = current_input
        
        np.savetxt(outfilepath, training_input, delimiter=";")
        
#%%
def label_prep(prepfilepath:str, focalfish:int, partnerfish:int,
              field_of_view:float, label_config:str, coordinate_system:str):
    
    datfiles = glob.glob(prepfilepath+"*_data.csv")
    for datfile in datfiles:
        filename = datfile.split("/")[-1].split("_data.csv")[0]
        outfilepath = prepfilepath+filename \
        +"_focalpos"+str(focalfish)+"_fov"+str(field_of_view)+"_"+coordinate_system+"_labels.csv"
        if os.path.exists(outfilepath):
            continue
            
        data = np.loadtxt(datfile, delimiter=";")
        focaltruth = data[:,focalfish*2:(focalfish*2)+2]
        partnercoords = data[:,partnerfish*2:(partnerfish*2)+2]
                
        focaldispx, focaldispy, focalmagnits, focalangles = fm.framewise_velocity(focaltruth[:,0], focaltruth[:,1])

        training_labels = np.empty((len(focaltruth), 2), float)
        training_labels[:] = np.nan

        
        first_frame = 0
        final_frame = len(focaltruth)-1
                
        for i in range(first_frame, final_frame):       
        
            ## LABELS
            if label_config in ["FollowTrack"]:
                bestmove_x = focaldispx[i+1]
                bestmove_y = focaldispy[i+1]
            elif label_config in ["FollowPartner"]:
                bestmove_x = partnercoords[i,0] - focaltruth[i,0]
                bestmove_y = partnercoords[i,1] - focaltruth[i,1]
                
            if coordinate_system == "polar":    
                best_dirchange = geom.atan2_regularizer(math.atan2(bestmove_y, bestmove_x) - focalangles[i])
                best_magnitude = math.sqrt(bestmove_x**2 + bestmove_y**2)
                training_labels[i,:] = np.array([[best_magnitude, best_dirchange]])      
            elif coordinate_system == "cartesian":
                training_labels[i,:] = np.array([fm.global_to_ego_rotation(bestmove_x, bestmove_y, focalangles[i])]) 

        np.savetxt(
            outfilepath, 
            training_labels, delimiter=";")    
        