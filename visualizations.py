#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 20:22:05 2023

@author: spathiphyllum
"""

import numpy as np
import pandas as pd
import glob
import math
import random
import os
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib
import matplotlib.animation as animation
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import sys
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

import Setup
sys.path.append(Setup.fish_movements_path)
import fish_movements as fm
import geometry_tools as geom

filenamepattern = "N09P4"
datafiles=glob.glob(prepfilepath+"*"+filenamepattern+"*.csv")

originaldata = np.loadtxt(datafiles[0], delimiter=";")
focaltruth = originaldata[:,0:2]
partner = originaldata[:,2:4]

corners, walls, tank = fm.tank_corners_walls(focaltruth, partner)

#%% Animations

sim_type = "sim_onesided"
animation = False

def core_plot(partner, focaltruth, simulation):
    if type(partner) != None:
        plt.plot(partner[:,0], partner[:,1], c="b", lw=0.5)
    if type(focaltruth) != None:
        plt.plot(focaltruth[:,0], focaltruth[:,1], c="g", lw=0.5)
    if type(simulation) != None:
        plt.plot(simulation[:,0], simulation[:,1], c="r", lw=0.5)
 
if animation:
    file_suffix = ".mp4"
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=10, metadata=dict(artist='Me'), bitrate=1800)
    def animate(i):
        core_plot(partner[:i,:], focaltruth[:i,:], tracks[:i,:])
else:
    file_suffix = ".png"
    
# sim_type = "sim_onesided"
# def animate(i):
#     plt.plot(partnercoords[:i,0], partnercoords[:i,1], c="b", lw=0.5)
#     plt.plot(focaltruth[:i,0], focaltruth[:i,1], c="g", lw=0.5)
    
# sim_type = "sim_double"
# def animate(i):
#     plt.plot(focalsim2[:i,0], focalsim2[:i,1], c="b", lw=0.5)
#     plt.plot(focalsim1[:i,0], focalsim1[:i,1], c="r", lw=0.5)

datfiles=glob.glob(outpath+"*"+sim_type+"*"+filenamepattern+"*.csv")

#datfile = datfiles[0]
for datfile in datfiles:
    entry = datfile.split("/")[-1].split(".csv")[0]
    motor_noise = entry.split("_")[-3]
    entry = entry.split("_with")[0]
    plotExists = os.path.exists(outpath+entry+"_"+motor_noise+"_motor_noise"+file_suffix)
    if not plotExists:
        print(entry)
        simulation = np.loadtxt(datfile, delimiter=";")
        if np.ndim(simulation) < 2:
            simulation = simulation.reshape(1, len(simulation))
        #focalsim1 = focalsim[:,0:2]
        #focalsim2 = focalsim[:,2:4]
                
        plt.close('all')
        fig=plt.figure(figsize=(5, 5), dpi= 100, facecolor="w", edgecolor="k")
        plt.box(on=False)
        #plt.xlim(-55,55)
        #plt.ylim(-55,55)
        #plt.title("Original data")
        plt.title(entry+"\n"+motor_noise+" motor noise")
        plt.plot(corners[:,0], corners[:,1], c="k", lw=0.5)
        if animation:
            ani = matplotlib.animation.FuncAnimation(fig, animate, frames=round(len(tracks)), repeat=True)
        else:
            core_plot(partner, focaltruth, simulation)
        #plt.show()
        #g_patch = mpatches.Patch(color="g", label="Real focal fish")
        #b_patch = mpatches.Patch(color="b", label="Real partner fish")
        #r_patch = mpatches.Patch(color="r", label="Simulated focal fish")
        #plt.legend(handles=[g_patch, b_patch, r_patch], loc="center", bbox_to_anchor=(0, -0.2))
        
        if animation:
            ani.save(outpath+entry+"_"+motor_noise+"_motor_noise.mp4", writer=writer)
        else:
            plt.savefig(outpath+entry+"_"+motor_noise+"_motor_noise.png", dpi=500)
        print("Plot finished")
        
        
        
        