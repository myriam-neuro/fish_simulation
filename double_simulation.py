#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 13 15:11:19 2023

@author: spathiphyllum
"""

import numpy as np
import glob
import math
import random
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import sys
import tqdm
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

#import config_local as config
from Setup import *
sys.path.append(fish_movements_path)
import fish_movements as fm
import geometry_tools as geom


#%% Double simulation

j=1
for entry in model_config:
    print(entry)    
    locals().update(model_config[entry])
    
    model1 = keras.models.load_model(outpath+"model_"+filenamepattern+"_"+entry)
    model2 = keras.models.load_model(outpath+"model_"+filenamepattern+"_"+entry)
    
    for motor_noise in ["with", "without"]:
        
        focalangle1 = focalangles[1]
        focalx1 = focaltruth[1,0] 
        focaly1 = focaltruth[1,1]
        dispx1 = focaldispx[1]
        dispy1 = focaldispy[1]
    
        focalangle2 = partnerangles[1]
        focalx2 = partnercoords[1,0] 
        focaly2 = partnercoords[1,1]
        dispx2 = partnerdispx[1]
        dispy2 = partnerdispy[1]
        
        focalsim1 = [focaltruth[1,:]]
        focalsim2 = [partnercoords[1,:]]
        print("Starting double simulation, model "+str(j)+" out of "+str(len(model_config))+" "+motor_noise+" motor noise.")
        
        for i in tqdm(range(1, 1000)): #len(focaltruth)-2):
            ## INPUT DATA
            
            if input_config == "Partner":
                current_input1 = fm.get_input(focalx1, focaly1, focalangle1, field_of_view, 
                                         coordinate_system, None, focalx2, focaly2, focalangle2)
                current_input2 = fm.get_input(focalx2, focaly2, focalangle2, field_of_view, 
                                         coordinate_system, None, focalx1, focaly1, focalangle1)
            if input_config == "WallsAndPartner":
                current_input1 = fm.get_input(focalx1, focaly1, focalangle1, field_of_view, 
                                         coordinate_system, walls, focalx2, focaly2, focalangle2)
                current_input2 = fm.get_input(focalx2, focaly2, focalangle2, field_of_view, 
                                         coordinate_system, walls, focalx1, focaly1, focalangle1)
            if input_config == "AllVisiWalls":
                current_input1 = fm.get_input(focalx1, focaly1, focalangle1, field_of_view, 
                                         coordinate_system, walls, None, None, None)
                current_input2 = fm.get_input(focalx2, focaly2, focalangle2, field_of_view, 
                                         coordinate_system, walls, None, None, None)
            
            
            ## MODEL PREDICTIONS                
            prediction1 = model1.predict(current_input1, verbose=0)

            if np.count_nonzero(np.isnan(prediction1)) > 0:
                break
            
            prediction2 = model2.predict(current_input2, verbose=0)
            if np.count_nonzero(np.isnan(prediction2)) > 0:
                break
                   
            dispx1, dispy1, focalangle1 = fm.prediction_to_movement(prediction1, coordinate_system, focalangle1)
            dispx2, dispy2, focalangle2 = fm.prediction_to_movement(prediction2, coordinate_system, focalangle2)
    
            if motor_noise=="with":
                dispx1 += np.random.normal(0, movement_noise_std/noise_divider, 1)
                dispy1 += np.random.normal(0, movement_noise_std/noise_divider, 1)
                dispx2 += np.random.normal(0, movement_noise_std/noise_divider, 1)
                dispy2 += np.random.normal(0, movement_noise_std/noise_divider, 1)
            nextpoint1 = Point(focalx1 + dispx1, focaly1 + dispy1)
            nextpoint2 = Point(focalx2 + dispx2, focaly2 + dispy2)
    
            if tank.contains(nextpoint1):
                focalx1 += dispx1
                focaly1 += dispy1     
            if tank.contains(nextpoint2):
                focalx2 += dispx2
                focaly2 += dispy2     
            focalsim1 = np.append(focalsim1, [[float(focalx1), float(focaly1)]], axis=0)
            focalsim2 = np.append(focalsim2, [[float(focalx2), float(focaly2)]], axis=0)
            focalsim = np.append(focalsim1, focalsim2, axis=1)
            
            if i%100==0:
                np.savetxt(outpath+"sim_double_"+filenamepattern+"_"+entry+"_"+motor_noise+
                           "_motor_noise.csv", focalsim, delimiter=";")
                
        np.savetxt(outpath+"sim_double_"+filenamepattern+"_"+entry+"_"+motor_noise+
                   "_motor_noise.csv", focalsim, delimiter=";")
        
        plt.close('all')
        fig=plt.figure(figsize=(5, 5), dpi= 100, facecolor="w", edgecolor="k")
        plt.box(on=False)
        plt.title(entry+"\nDouble sim "+motor_noise+" motor noise")
        plt.plot(corners[:,0], corners[:,1], c="k", lw=0.5)
        plt.plot(focalsim1[:,0], focalsim1[:,1], c="b", lw=0.5)
        plt.plot(focalsim2[:,0], focalsim2[:,1], c="r", lw=0.5)
        plt.savefig(outpath+"sim_double_"+filenamepattern+"_"+entry+"_"+motor_noise+"_motor_noise.png", dpi=500)
        
        print("Finished double simulation")
        j += 1