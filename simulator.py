# -*- coding: utf-8 -*-
"""
Created Feb 2023

@author: %Lea Musiolek
"""

# ### Initialization:

import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import random
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
import sys
from tqdm import tqdm

import setup
sys.path.append(setup.fish_movements_path)
import fish_movements as fm
import geometry_tools as geom
import data_handling as dh
import controller_rnn as cont_rnn
from shapely.geometry import Point

import os
import glob


#%% MLP simulator
def mlp_simulator(random_seed, input_nodes, hidden_layers, coordinate_system, fish_range_1d):
    '''
    Function for setting up an MLP given the hyperparameters.

    Parameters
    ----------
    random_seed : TYPE
        DESCRIPTION.
    input_nodes : TYPE
        DESCRIPTION.
    model_layers : TYPE
        DESCRIPTION.
    coordinate_system : TYPE
        DESCRIPTION.
    fish_range_1d : TYPE
        DESCRIPTION.

    Returns
    -------
    TYPE
        DESCRIPTION.

    '''
    random.seed(random_seed)
    np.random.seed(random_seed)
    tf.random.set_seed(random_seed)
    
    inputs = keras.Input(shape = (input_nodes,))
    hiddenlayer = inputs
    for hl in hidden_layers:
        if hl > 0:
            hiddenlayer = layers.Dense(hl, activation = layers.LeakyReLU(alpha=0.01))(hiddenlayer)
    
    if coordinate_system == "cartesian":
        # def relu_clipped(x):
        #     return keras.backend.relu(x+fish_range_1d, alpha=0, threshold=0.0, max_value=fish_range_1d*2) - fish_range_1d
        # out_x = layers.Dense(1, activation = relu_clipped)(hiddenlayer)
        # out_y = layers.Dense(1, activation = relu_clipped)(hiddenlayer)
        out_x = layers.Dense(1, activation = None)(hiddenlayer)
        out_y = layers.Dense(1, activation = None)(hiddenlayer)
    if coordinate_system == "polar":
        # def relu_clipped(x):
        #     return keras.backend.relu(x, alpha=0, max_value=math.sqrt(2*(fish_range_1d**2)), threshold=0.0)
        # out_x = layers.Dense(1, activation = relu_clipped)(hiddenlayer)
        out_x = layers.Dense(1, activation = "relu")(hiddenlayer)
        # def tanh_to_atan2(x):
        #     return keras.backend.tanh(x)*np.pi
        # out_y = layers.Dense(1, activation = tanh_to_atan2)(hiddenlayer)
        out_y = layers.Dense(1, activation = None)(hiddenlayer)
                    
    
    outputs = layers.Concatenate(axis=1)([out_x, out_y])
    
    model = keras.Model(inputs=inputs, outputs=outputs, name="fish_model")
    model.summary()
    
    opt = keras.optimizers.Adam(clipnorm=2.0)
    model.compile(loss="mean_squared_error", optimizer=opt)
    return model

#%% Loop over configurations

def model_train(genome, key, run, prepfilepath, outpath, focalfish, partnerfish, label_config, coordinate_system, field_of_view, fish_range_1d, testpart, save_model:bool):
        
    # Preprocess data
    for partner_timestep in genome["partner_timesteps"]:
        dh.partnerinput_prep(prepfilepath, focalfish, partnerfish, partner_timestep, field_of_view, coordinate_system)
    if genome["wall_vision"]:
        dh.wallinput_prep(prepfilepath, focalfish, partnerfish, field_of_view, coordinate_system, True)
    dh.label_prep(prepfilepath, focalfish, partnerfish, field_of_view, label_config, coordinate_system)
    
    #%% Controller setup
    
    input_nodes = len(genome["partner_timesteps"])*2 + int(genome["wall_vision"])*8
    
    model_path = outpath+"model_run_"+run+"_genome_"+str(key)
    if os.path.exists(model_path):
        print("Model already exists, loading now.")
        model = keras.models.load_model(model_path)
    else:   
        model = mlp_simulator(genome["random_seed"], input_nodes, genome["hidden_layers"], coordinate_system, fish_range_1d)
        #keras.utils.plot_model(model, model_path+".png", show_shapes=True, show_layer_activations=True)

    callback = keras.callbacks.EarlyStopping(monitor="val_loss", patience=5, mode="auto")

    # if model_arch.split("_")[0] == "RNN":
    #     rnn_timewindow = int(model_arch.split("_")[1])

    #     #restructure the data :
    #     training_input, training_labels = cont_rnn.data_rnn(training_input, rnn_timewindow, training_labels)

    #     # BUILD MODEL HERE = call a function that is to be created in the controller
    #     model = cont_rnn.rnn_controller(rnn_timewindow, random_seed, input_nodes, model_layers, nodes_per_layer, coordinate_system, fish_range_1d)

    
    #%% Training
    
    # Load data
    trainfilelist = []
    datfiles = glob.glob(prepfilepath+"*_data.csv")
    for datfile in datfiles:
        filename = datfile.split("/")[-1].split("_data.csv")[0]
        trainfilelist.append(filename)
        
        labelpath = prepfilepath+filename \
        +"_focalpos"+str(focalfish)+"_fov"+str(field_of_view)+"_"+coordinate_system+"_labels.csv"
        labels = np.loadtxt(labelpath, delimiter=";")
        
        inputs = np.empty((len(labels), 0))
        for partner_timestep in genome["partner_timesteps"]:
            partnerpath = prepfilepath+filename+"_partner_ts"+str(partner_timestep) \
            +"_focalpos"+str(focalfish)+"_fov"+str(field_of_view)+"_"+coordinate_system+"_partnerinput.csv"
            partner_input = np.loadtxt(partnerpath, delimiter=";")
            inputs = np.append(inputs, partner_input, axis=1)
        
        if genome["wall_vision"]:
            wallpath = prepfilepath+filename+"_walls_fov"+str(field_of_view)+"_"+coordinate_system+"_wallinput.csv"
            wall_input = np.loadtxt(wallpath, delimiter=";")
            inputs = np.append(inputs, wall_input, axis=1)

        if not len(labels) == len(inputs):
            print("Inputs and labels not same length")
            continue
        
        # Remove nans
        inputnonans = ~np.isnan(inputs).any(axis=1)
        inputs = inputs[inputnonans, :]
        labels = labels[inputnonans, :]

        if not len(labels) == len(inputs):
            print("Inputs and labels not same length")
            continue
        labelnonans = ~np.isnan(labels).any(axis=1)
        inputs = inputs[labelnonans, :]
        labels = labels[labelnonans, :]
        
        # Split into training and test data, write out test data for later
        testslice_a = round(0.5*len(inputs))
        testslice_b = testslice_a + round(testpart*len(inputs))
        training_input = np.append(inputs[0:testslice_a,:], inputs[testslice_b:,:], axis=0)
        if testpart > 0:
            test_input = inputs[testslice_a:testslice_b,:]
            np.savetxt(outpath+filename+"_test_input.csv", test_input, delimiter=";")

        training_labels = np.append(labels[0:testslice_a,:], labels[testslice_b:,:], axis=0)
        if testpart > 0:
            test_labels = labels[testslice_a:testslice_b,:]
            np.savetxt(outpath+filename+"_test_labels.csv", test_labels, delimiter=";")

        for col in range(0,training_input.shape[1]):
            noise = np.random.normal(0, np.std(training_input[:,col])*genome["input_noise"], len(training_input))
            if coordinate_system == "polar" and col%2 == 1:
                for k in range(0, len(training_input)):
                    training_input[k,col] = geom.atan2_regularizer(training_input[k,col] + noise[k])
            else:
                training_input[:,col] += noise
        
        # Train
        model.fit(x=training_input, y=training_labels, validation_split=0.1, epochs=50, verbose=0, callbacks=[callback])
            
    np.savetxt(outpath+"Trainfiles_run_"+run+".csv", np.array(trainfilelist), delimiter=";", fmt='%s')

    if save_model:
        model.save(model_path)
    print("Training completed for model "+str(key))
    
    return model, model.count_params()
    
#%% Testing
    
def model_test_prelabeled(model, outpath):
    test_losses = []
    
    # Load data
    testfiles = glob.glob(outpath+"*_test_input.csv")
    for testfile in testfiles:
        test_input = np.loadtxt(testfile, delimiter=";")
        test_labels = np.loadtxt(testfile.split("_test_input.csv")[0]+"_test_labels.csv", delimiter=";")
        
        test_loss = model.evaluate(test_input, test_labels, verbose=0)
        test_losses.append(test_loss)
        
        os.remove(testfile)
        os.remove(testfile.split("_test_input.csv")[0]+"_test_labels.csv")
        
    return np.mean(test_losses)

def model_test_sim_discrim(model, genome, key, run, prepfilepath, outpath, discrim_path, focalfish, partnerfish, 
                           coordinate_system, motor_noise, field_of_view, simtype):
    if simtype == "onesided":
        simulation_onesided(model, genome, key, run, prepfilepath, outpath, focalfish, partnerfish, 
                            coordinate_system, motor_noise, field_of_view)
    
    avgscore = get_simulation_discriminator("_run_"+run+"_genome_"+str(key)+"_"+motor_noise, prepfilepath, outpath, discrim_path, partnerfish, simtype)
    return -avgscore

#%% Simulation

def simulation_onesided(model, genome, key, run, prepfilepath, outpath, focalfish, partnerfish, coordinate_system, motor_noise, field_of_view):        
    # Load data
    datfiles = glob.glob(prepfilepath+"*_data.csv")
    for datfile in datfiles:
        filename = datfile.split("/")[-1].split("_data.csv")[0]
        
        data = np.loadtxt(datfile, delimiter=";")
        focaltruth = data[:,focalfish*2:(focalfish*2)+2]
        partnercoords = data[:,partnerfish*2:(partnerfish*2)+2]
        
        first_frame = 0
        final_frame = len(partnercoords)-1

        if np.min(genome["partner_timesteps"]) < 0:
            first_frame -= np.min(genome["partner_timesteps"])
        elif np.max(genome["partner_timesteps"]) > 0:
            final_frame -= np.max(genome["partner_timesteps"])
        
        if genome["wall_vision"]:
            if os.path.exists(prepfilepath+filename+"_walls_general.csv"):
                walls = np.loadtxt(prepfilepath+filename+"_walls_general.csv", delimiter=";")
            else:
                corners, walls, tank = fm.tank_corners_walls(focaltruth, partnercoords)
                np.savetxt(prepfilepath+filename+"_walls_general.csv", np.array(walls), delimiter=";")

        partnerdispx, partnerdispy, partnermagnit, partnerangles = fm.framewise_velocity(partnercoords[:,0], partnercoords[:,0])
        focalangle = partnerangles[first_frame]
        focalx = partnercoords[first_frame,0] 
        focaly = partnercoords[first_frame,1]
                
        focalsim = [partnercoords[first_frame,:]]
        movement_noise_std = (np.std(partnerdispx) + np.std(partnerdispy))/2
        
        print("Starting one-sided simulation, "+motor_noise+" motor noise.")
        
        for i in tqdm(range(first_frame, final_frame)):
                    
            ## INPUT DATA
            current_input = np.empty((0, 0))
            for partner_timestep in genome["partner_timesteps"]:
                partner_input = fm.get_partner_input(focalx, focaly, focalangle, field_of_view, 
                                          coordinate_system, partnercoords[i+partner_timestep, 0], 
                                          partnercoords[i+partner_timestep, 1], partnerangles[i+partner_timestep])
                np.append(current_input, partner_input, axis=1)
                
            if genome["wall_vision"]:
                wall_input = fm.get_wall_input(focalx, focaly, focalangle, field_of_view, coordinate_system, walls)
                np.append(current_input, wall_input, axis=1)

            # MODEL PREDICTIONS                
            prediction = model.predict(current_input, verbose=0)
    
            # if np.count_nonzero(np.isnan(prediction)) > 0:
            #     print("NaNs in prediction")
            #     break
    
            dispx, dispy, focalangle = fm.prediction_to_movement(prediction, coordinate_system, focalangle)
        
            if motor_noise == "with":
                dispx += np.random.normal(0, movement_noise_std/8, 1)
                dispy += np.random.normal(0, movement_noise_std/8, 1)
                
            nextpoint = Point(focalx + dispx, focaly + dispy)
            if tank.contains(nextpoint):
                focalx += dispx
                focaly += dispy
            
            focalsim = np.append(focalsim, [[float(focalx), float(focaly)]], axis=0)
            
            if i%100==0:
                simpath = outpath+"sim_onesided_"+filename+"_run_"+run+"_genome_"+str(key)+"_"+motor_noise+"_motor_noise.csv"
                np.savetxt(simpath, focalsim, delimiter=";")
                
        if len(focalsim) > 1:        
            np.savetxt(simpath, focalsim, delimiter=";")
            print("Finished one-sided simulation")
                
    return

#%% Rate simulations by a discriminator

def get_simulation_discriminator(simname, prepfilepath, outpath, discrim_path, partnerfish, simtype):
    discrim = keras.models.load_model(discrim_path, compile=False)
    discrim.compile()
    sequence_size = discrim.layers[0].input_shape[1]
    
    scores = []
    simfiles = glob.glob(outpath+"sim_"+simtype+"_*"+simname+"*")
    for simfile in simfiles:
        filename = simfile.split("sim_"+simtype+"_")[1].split("_run_")[0]
        data = np.loadtxt(prepfilepath+filename+"_data.csv", delimiter=";")
        partnercoords = data[:,partnerfish*2:(partnerfish*2)+2]

        simulation = np.loadtxt(simfile, delimiter=";")
        
        if len(partnercoords) == len(simulation):
            data_points = len(simulation)
        else:
            print("Simulation and partner coords not of same length")
            break
        
        discrim_input = np.empty((int(data_points / sequence_size), sequence_size, 4))
        i = 0
        for l in range(0, simulation.shape[0], sequence_size):
            discrim_input[i][:, 0:2] = simulation[l:l + sequence_size,:]
            discrim_input[i][:, 2:4] = partnercoords[l:l + sequence_size,:]
            i += 1

        score = discrim.predict(discrim_input)
        scores.append(score)
    avgscore = np.mean(scores)
    return avgscore
